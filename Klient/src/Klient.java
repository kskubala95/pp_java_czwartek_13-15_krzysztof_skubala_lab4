import javax.swing.*;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;

/**
 * Created by Krzysztof on 2017-04-05.
 */
public class Klient extends JApplet {

// ***********************************************************************
//  LicznikGlosow.java
//
//  Wykorzystuje GUI oraz listenery eventow do glosowania
//  na dwoch kandydatow -- Jacka i Placka.
//
// ***********************************************************************

    private int APPLET_WIDTH = 300, APPLET_HEIGHT = 100;
    private int glosyDlaJacek, glosyDlaPlacek, glos;
    private JLabel labelJacek, labelPlacek,labelWyjdz;
    private JButton Jacek, Placek,Wyjdz;
    private Socket strumien;
    private DataInputStream in;
    private InputStream in_sock;


    // ------------------------------------------------------------
    //  Ustawia GUI
    // ------------------------------------------------------------

    public void init ( ) {
        try {
            System.out.println ( "Proba nawiazania polaczenia z serwerem... " );
            String ip = "localhost";
            int port = 2222;
            strumien = new Socket ( ip, port );
            System.out.println ( "Klient polaczyl sie do servera. " );
            in_sock = strumien.getInputStream ( );
            in = new DataInputStream ( in_sock );
        } catch ( IOException e ) {
            System.err.println ( e.getMessage ( ) );
            e.printStackTrace ( );
            System.out.println ( "Nie udalo sie nawiazac polaczenia z serverem ! " );
        }

        glosyDlaJacek = 0;
        glosyDlaPlacek = 0;

        Jacek = new JButton ( "Glosuj na Jacka!" );
        Jacek.addActionListener ( new JacekButtonListener ( ) );

        Wyjdz = new JButton ( "Wyjscie!" );
        Wyjdz.addActionListener ( new WyjdzButtonListener ( ) );

        Placek = new JButton ( "Glosuj na Placka!" );
        Placek.addActionListener ( new PlacekButtonListener ( ) );


        labelJacek = new JLabel ( "Glosy dla Jacka: " + Integer.toString ( glosyDlaJacek ) );
        labelPlacek = new JLabel ( "Glosy dla Placka: " + Integer.toString ( glosyDlaPlacek ) );


        Container cp = getContentPane ( );
        cp.setBackground ( Color.cyan );
        cp.setLayout ( new FlowLayout ( ) );
        cp.add ( Jacek );
        cp.add ( labelJacek );

        cp.add ( Placek );
        cp.add ( labelPlacek );

        cp.add ( Wyjdz );

        setSize ( APPLET_WIDTH, APPLET_HEIGHT );
    }

    public void Send ( int x ) {
        try {
            DataOutputStream out;
            OutputStream out_sock;
            out_sock = strumien.getOutputStream ( );
            out = new DataOutputStream ( out_sock );
            out.writeInt ( x );
            out.flush ( );
            System.out.println ( "Wysłano wartosc na server" );
        } catch ( IOException e ) {
            System.err.println ( e.getMessage ( ) );
            e.printStackTrace ( );
            System.out.println ( "Wysylanioe pakietow nie powiodlo sie.." );
        }
    }


    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku
    // *******************************************************************
    private class JacekButtonListener implements ActionListener {
        public void actionPerformed ( ActionEvent event ) {
            glos =1;
            Send ( glos );
            try {
                labelJacek.setText ( "Glosy dla Jacka: " + in.readInt ( ) );
            }
            catch (IOException e)
            {
                System.err.println ( e.getMessage ( ) );
                e.printStackTrace ( );
            }
            repaint ( );
        }
    }

    private class WyjdzButtonListener implements ActionListener {
        public void actionPerformed ( ActionEvent event ) {
            glos =-1;
            Send ( glos );
            try {
                strumien.close ( );

            } catch (IOException e)
            {
                System.err.println ( e.getMessage ( ) );
                e.printStackTrace ( );
            }
            System.exit ( 1 );
        }
    }

    private class PlacekButtonListener implements ActionListener {
        public void actionPerformed ( ActionEvent event ) {
            glos =2;
            Send ( glos );
            try {
                labelPlacek.setText ( "Glosy dla Placka: " + in.readInt ( ) );
            }
            catch (IOException e)
            {
                System.err.println ( e.getMessage ( ) );
                e.printStackTrace ( );
            }
            repaint ( );
        }
    }
}