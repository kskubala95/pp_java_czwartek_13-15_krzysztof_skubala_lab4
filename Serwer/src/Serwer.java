/**
 * Created by Krzysztof on 2017-04-05.
 */
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
public class Serwer extends Thread{

    static final int PORT = 2222;
    public static void main ( String args[] ) throws IOException {

        ServerSocket serverSocket = null;
        Socket socket = null;
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                System.out.println("Error: " + e);
            }
            new EchoThread(socket).start();
        }

        }
    }


