/**
 * Created by Krzysztof on 2017-04-05.
 */
import java.io.*;
import java.net.Socket;
public class EchoThread extends Thread  {
    protected Socket socket;
    public static int glosyDlaJacek, glosyDlaPlacek;
    public EchoThread(Socket clientSocket) {
        this.socket = clientSocket;
    }

    public void run() {
        int v = 0;
        while (v != -1)
        {
            try {
                DataInputStream in;
                InputStream in_sock;
                in_sock = socket.getInputStream();
                in = new DataInputStream(in_sock);
                v = in.readInt();
                System.out.println("Odebrano: " + v);

            } catch (IOException e) {
                System.err.println(e.getMessage());
                e.printStackTrace();
                try {
                    socket.close();
                } catch (IOException f) {
                    System.err.println(f.getMessage());
                    f.printStackTrace();
                }
            }
            try {
                DataOutputStream out;
                OutputStream out_sock;
                out_sock = socket.getOutputStream();
                out = new DataOutputStream(out_sock);
                if (v == 1) {
                    glosyDlaJacek++;
                    out.writeInt(glosyDlaJacek);
                }
                if (v == 2) {
                    glosyDlaPlacek++;
                    out.writeInt(glosyDlaPlacek);
                }
                if(v==3)
                {
                    out.writeInt(glosyDlaJacek);
                }
                if(v==4)
                {
                    out.writeInt(glosyDlaPlacek);
                }
                out.flush();
                System.out.println("Wysłano wartosc");
            } catch (IOException e) {
                System.err.println(e.getMessage());
                e.printStackTrace();
                try {
                    socket.close();

                } catch (IOException f) {
                    System.err.println(f.getMessage());
                    f.printStackTrace();
                }
            }
        }
    }
}